//
//  MissionContentView.swift
//  lightMaze
//
//  Created by aby on 2019/11/15.
//  Copyright © 2019 aby. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

struct MissionContentView: View {
    let viewModel: MissionViewModel
    init(viewModel: MissionViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        VStack {
            headerViewStack()
            bodyView()
        }.frame(alignment: .top)
        .background(Color.white)
    }
    
    func headerViewStack() -> some View {
        return HStack {
            button(title: "退出", color: Color.red) {
                // 退出事件
                self.viewModel.exit()
            }
            Spacer()
            Text("第一关")
            Spacer()
            button(title: "重置", color: Color.green) {
                // 提示事件
            }
            button(title: "提示", color: Color.green) {
                // 重置事件
            }
        }.padding(.horizontal, 15)
    }
    
    func bodyView() -> some View {
        return VStack {
            HStack {
                Text("\(viewModel.timeString)")
                    .font(.system(size: 45))
                    .foregroundColor(Color.black)
                Spacer()
                Text("\(viewModel.clickTimes)步")
                    .font(.system(size: 44))
                    .foregroundColor(Color.black)
                
            }.padding(20)
            ForEach(0..<viewModel.lights.count) { row in
                HStack(spacing: 20) {
                    ForEach(0..<self.viewModel.lights[row].count) { column in
                        Circle()
                            .foregroundColor(self.viewModel.lights[row][column].status ? .yellow : .gray)
                            .opacity(self.viewModel.lights[row][column].status ? 0.8 : 0.5)
                            .frame(width: self.viewModel.circleWidth(),
                                   height: self.viewModel.circleWidth())
                            .shadow(color: .yellow, radius: self.viewModel.lights[row][column].status ? 10 : 0)
                            .onTapGesture {
                                self.viewModel.clickTimes += 1
                                self.viewModel.updateLightStatus(column: column, row: row)
                        }
                    }
                }
                .padding(EdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0))
            }.padding(20)
        }
    }
    
    func button(title: String, color: Color, action:@escaping () -> Void) -> some View {
        return ZStack {
            Rectangle()
                .frame(width: 60, height: 35, alignment: .center)
                .foregroundColor(color)
                .cornerRadius(15)
            Text(title)
            .bold()
        }.onTapGesture {
            action()
        }
    }
}

struct MissionContent_preview: PreviewProvider {
    static var previews: some View {
        let viewModel = MissionViewModel.init(pop: MainNavViewModel.init(), size: 6)
        return Group {
            MissionContentView.init(viewModel: viewModel)
        }
    }
}
