//
//  SetpViewModel.swift
//  lightMaze
//
//  Created by aby on 2019/11/15.
//  Copyright © 2019 aby. All rights reserved.
//

import Foundation

class SetpViewModel {
    private let randomRedponder: NavigateToRandom
    private let missonResponder: NavigateToMission
    init(toRandom: NavigateToRandom, missonResponder: NavigateToMission) {
        self.randomRedponder = toRandom
        self.missonResponder = missonResponder
    }
    
    func toRandomPage() {
        self.randomRedponder.toRandom()
    }
    
    func toMissionPage() {
        self.missonResponder.toMission()
    }
}
